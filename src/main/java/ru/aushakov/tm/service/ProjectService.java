package ru.aushakov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.exception.empty.EmptyProjectIdException;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.exception.general.NoUserLoggedInException;
import ru.aushakov.tm.model.Project;

import java.util.Optional;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    @NonNull
    private final IProjectRepository projectRepository;

    @NonNull
    private final ITaskRepository taskRepository;

    public ProjectService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final Class currentClass
    ) {
        this.repository = projectRepository;
        this.businessRepository = projectRepository;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.currentClass = currentClass;
    }

    @Override
    @NotNull
    public Project deepDeleteProjectById(@Nullable final String projectId, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(projectId)) throw new EmptyProjectIdException();
        @NotNull final Project deletedProject = Optional
                .ofNullable(projectRepository.removeOneById(projectId, userId))
                .orElseThrow(ProjectNotFoundException::new);
        taskRepository.removeAllTasksByProjectId(projectId, userId);
        return deletedProject;
    }

}
