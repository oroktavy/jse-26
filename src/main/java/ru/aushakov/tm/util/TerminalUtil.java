package ru.aushakov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.exception.general.InvalidIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull Exception e) {
            throw new InvalidIndexException(value);
        }
    }

}
