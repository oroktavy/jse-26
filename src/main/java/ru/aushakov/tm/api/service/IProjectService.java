package ru.aushakov.tm.api.service;

import ru.aushakov.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project deepDeleteProjectById(String projectId, String userId);

}
