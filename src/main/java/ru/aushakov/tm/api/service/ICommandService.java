package ru.aushakov.tm.api.service;

import ru.aushakov.tm.api.ServiceLocator;
import ru.aushakov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getTerminalCommands();

    AbstractCommand getCommandByName(String command);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);

    void initCommands(ServiceLocator serviceLocator);

}
