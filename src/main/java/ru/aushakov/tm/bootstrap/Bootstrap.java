package ru.aushakov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.ServiceLocator;
import ru.aushakov.tm.api.repository.ICommandRepository;
import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.api.service.*;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.general.UnknownArgumentException;
import ru.aushakov.tm.exception.general.UnknownCommandException;
import ru.aushakov.tm.model.Project;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.repository.CommandRepository;
import ru.aushakov.tm.repository.ProjectRepository;
import ru.aushakov.tm.repository.TaskRepository;
import ru.aushakov.tm.repository.UserRepository;
import ru.aushakov.tm.service.*;
import ru.aushakov.tm.util.SystemUtil;
import ru.aushakov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, Task.class);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository, Project.class);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userRepository, propertyService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    private void initData() {
        userService.add("TEST", "TEST", "test@tsconsulting.com");
        userService.add("ADMIN", "ADMIN", "admin@tsconsulting.com", "admin");

        @NotNull final String testId = userService.findOneByLogin("TEST").getId();
        @NotNull final String adminId = userService.findOneByLogin("ADMIN").getId();
        taskService.add("DEMO 2", "222222222222", testId).setStatus(Status.IN_PROGRESS);
        taskService.add("DEMO 3", "333333333333", testId);
        taskService.add("DEMO 1", "111111111111", adminId).setStatus(Status.COMPLETED);
        projectService.add("DEMO 2", "222222222222", adminId).setStatus(Status.IN_PROGRESS);
        projectService.add("DEMO 3", "333333333333", testId);
        projectService.add("DEMO 1", "111111111111", adminId).setStatus(Status.COMPLETED);
    }

    @SneakyThrows
    private void createPIDFile() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void login() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("ENTER THE MATRIX");
    }

    public void run(@Nullable final String[] args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        commandService.initCommands(this);
        createPIDFile();
        initData();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                if (!authService.isUserAuthenticated()) login();
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArg(@Nullable final String arg) {
        @NotNull final AbstractCommand command = Optional
                .ofNullable(commandService.getCommandByArg(arg))
                .orElseThrow(() -> new UnknownArgumentException(arg));
        command.execute();
    }

    public void parseCommand(@Nullable final String command) {
        @NotNull final AbstractCommand parsedCommand = Optional
                .ofNullable(commandService.getCommandByName(command))
                .orElseThrow(() -> new UnknownCommandException(command));
        authService.checkRoles(parsedCommand.getRoles());
        parsedCommand.execute();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        if (args.length > 1) System.out.println("NOTE: Only one argument is supported at a time!");
        return true;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

}
