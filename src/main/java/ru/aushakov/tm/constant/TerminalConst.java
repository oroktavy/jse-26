package ru.aushakov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface TerminalConst {

    @NotNull String CMD_VERSION = "version";

    @NotNull String CMD_ABOUT = "about";

    @NotNull String CMD_HELP = "help";

    @NotNull String CMD_EXIT = "exit";

    @NotNull String CMD_INFO = "info";

    @NotNull String CMD_ARGUMENTS = "arguments";

    @NotNull String CMD_COMMANDS = "commands";

    @NotNull String CMD_VIEW_PROFILE = "view-profile";

    @NotNull String CMD_CHANGE_PASSWORD = "change-password";

    @NotNull String CMD_LOGOUT = "lolgout";

    @NotNull String CMD_LOGIN = "login";

    @NotNull String TASK_CREATE = "task-create";

    @NotNull String TASK_CLEAR = "task-clear";

    @NotNull String TASK_LIST = "task-list";

    @NotNull String TASK_VIEW_BY_ID = "task-view-by-id";

    @NotNull String TASK_VIEW_BY_INDEX = "task-view-by-index";

    @NotNull String TASK_VIEW_BY_NAME = "task-view-by-name";

    @NotNull String TASK_REMOVE_BY_ID = "task-remove-by-id";

    @NotNull String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    @NotNull String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    @NotNull String TASK_UPDATE_BY_ID = "task-update-by-id";

    @NotNull String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    @NotNull String TASK_START_BY_ID = "task-start-by-id";

    @NotNull String TASK_START_BY_INDEX = "task-start-by-index";

    @NotNull String TASK_START_BY_NAME = "task-start-by-name";

    @NotNull String TASK_FINISH_BY_ID = "task-finish-by-id";

    @NotNull String TASK_FINISH_BY_INDEX = "task-finish-by-index";

    @NotNull String TASK_FINISH_BY_NAME = "task-finish-by-name";

    @NotNull String TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";

    @NotNull String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";

    @NotNull String TASK_CHANGE_STATUS_BY_NAME = "task-change-status-by-name";

    @NotNull String TASK_ASSIGN_TO_PROJECT = "task-assign-to-project";

    @NotNull String TASK_UNBIND_FROM_PROJECT = "task-unbind-from-project";

    @NotNull String TASK_FIND_ALL_BY_PROJECT_ID = "task-find-all-by-project-id";

    @NotNull String PROJECT_CREATE = "project-create";

    @NotNull String PROJECT_CLEAR = "project-clear";

    @NotNull String PROJECT_LIST = "project-list";

    @NotNull String PROJECT_VIEW_BY_ID = "project-view-by-id";

    @NotNull String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    @NotNull String PROJECT_VIEW_BY_NAME = "project-view-by-name";

    @NotNull String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    @NotNull String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    @NotNull String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    @NotNull String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    @NotNull String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    @NotNull String PROJECT_START_BY_ID = "project-start-by-id";

    @NotNull String PROJECT_START_BY_INDEX = "project-start-by-index";

    @NotNull String PROJECT_START_BY_NAME = "project-start-by-name";

    @NotNull String PROJECT_FINISH_BY_ID = "project-finish-by-id";

    @NotNull String PROJECT_FINISH_BY_INDEX = "project-finish-by-index";

    @NotNull String PROJECT_FINISH_BY_NAME = "project-finish-by-name";

    @NotNull String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";

    @NotNull String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";

    @NotNull String PROJECT_CHANGE_STATUS_BY_NAME = "project-change-status-by-name";

    @NotNull String PROJECT_DEEP_DELETE_BY_ID = "project-deep-delete-by-id";

    @NotNull String USER_CREATE = "user-create";

    @NotNull String USER_CREATE_WITH_ROLE = "user-create-with-role";

    @NotNull String USER_REMOVE_BY_ID = "user-remove-by-id";

    @NotNull String USER_REMOVE_BY_LOGIN = "user-remove-by-login";

    @NotNull String USER_UPDATE_BY_ID = "user-update-by-id";

    @NotNull String USER_UPDATE_BY_LOGIN = "user-update-by-login";

    @NotNull String USER_SET_PASSWORD = "user-set-password";

    @NotNull String USER_LOCK_BY_LOGIN = "user-lock-by-login";

    @NotNull String USER_UNLOCK_BY_LOGIN = "user-unlock-by-login";

}
